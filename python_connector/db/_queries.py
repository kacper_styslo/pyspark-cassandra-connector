from . import _cassandra


@_cassandra.db_operator
def __create_keyspace() -> str:
    return """CREATE KEYSPACE IF NOT EXISTS 
              projects WITH 
              REPLICATION = {'class': 'SimpleStrategy', 'replication_factor': 2};"""


@_cassandra.db_operator
def __create_table(table_name) -> str:
    return f"""
        CREATE
        TABLE
        IF
        NOT
        EXISTS
        projects.{table_name}
        (
        id         int,
        name       text,
        technology text,
        salary     double,
        PRIMARY KEY (id));"""
