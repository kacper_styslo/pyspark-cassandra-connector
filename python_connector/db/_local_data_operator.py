from os import path

from cassandracsv import CassandraCsv
from cassandra.cluster import Cluster, Session as CassandraSession


def __save_data_form_cassandra_locally(table_name: str, output_name: str) -> None:
    """
    This module save data from cassandra locally in csv format.
    """
    cluster: Cluster = Cluster(["127.0.0.1"])
    session: CassandraSession = cluster.connect()
    session.execute("USE projects;")
    raw_data = session.execute(f"SELECT * FROM {table_name};")
    CassandraCsv.export(
        result=raw_data,
        output_dir=path.join(path.dirname(path.realpath(__file__)), "data"),
        filename=output_name,
    )


def __insert_some_local_data_into_cassandra() -> None:
    cluster: Cluster = Cluster(["127.0.0.1"])
    session: CassandraSession = cluster.connect()
    session.execute("USE projects;")
    with open(
        path.join(path.dirname(path.realpath(__file__)), "data", "data_to_insert.csv"),
        "r",
        encoding="utf-8",
    ) as prepared_data:
        for line in prepared_data:
            splited_line = line.split(",")
            session.execute(
                f"INSERT INTO {splited_line[0]} (id, name, technology, salary) "
                f"VALUES ({int(splited_line[1])}, '{str(splited_line[2])}',"
                f"'{str(splited_line[4])}',{float(splited_line[3])});"
            )
