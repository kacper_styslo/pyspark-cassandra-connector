from ._cassandra import wait_for_cassandra

from ._queries import __create_table, __create_keyspace

from ._local_data_operator import (
    __save_data_form_cassandra_locally,
    __insert_some_local_data_into_cassandra,
)


def prepare_cassandra() -> None:
    __create_keyspace()
    __create_table("programmers")
    __create_table("devops")
    __insert_some_local_data_into_cassandra()
    __save_data_form_cassandra_locally(table_name="programmers", output_name="programmers_data")
    __save_data_form_cassandra_locally(table_name="devops", output_name="devops_data")
