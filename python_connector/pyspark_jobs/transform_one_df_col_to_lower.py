from pyspark.sql import DataFrame
from pyspark.sql import functions as f

# Our
from _jobs_operators import PROGRAMMERS_DF


@f.udf()
def to_lower(technology: str) -> str:
    return technology.lower()


def transform_one_df_col_to_lower() -> None:
    PROGRAMMERS_DF.show()
    programmers_df: DataFrame = PROGRAMMERS_DF.withColumn(
        "technology lower case", to_lower(PROGRAMMERS_DF["technology"])
    )
    programmers_df.show()


transform_one_df_col_to_lower()
