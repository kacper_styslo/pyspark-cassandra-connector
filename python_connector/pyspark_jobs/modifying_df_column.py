from pyspark.sql import DataFrame
from pyspark.sql import functions as f
from typing import Tuple

# Our
from _jobs_operators import DEVOPS_DF, PROGRAMMERS_DF


def modifying_df_column(
    programmers_df: DataFrame, devops_df: DataFrame
) -> Tuple[DataFrame, DataFrame]:

    programmers_df = programmers_df.withColumn(
        "splited_names", f.split(programmers_df["name"], "\s+")
    )
    programmers_df = programmers_df.withColumn(
        "first_name", programmers_df["splited_names"].getItem(0)
    )
    programmers_df = programmers_df.withColumn(
        "last_name", programmers_df["splited_names"].getItem(1)
    )
    devops_df = devops_df.withColumn("splited_names", f.split(devops_df["name"], "\s+"))
    devops_df = devops_df.withColumn("first_name", devops_df["splited_names"].getItem(0))
    devops_df = devops_df.withColumn("last_name", devops_df["splited_names"].getItem(1))
    columns_to_drop: Tuple[str, str] = (
        "name",
        "splited_names",
    )
    programmers_df = programmers_df.drop(*columns_to_drop)
    devops_df = devops_df.drop(*columns_to_drop)
    programmers_df.show()
    devops_df.show()

    return devops_df, programmers_df


modifying_df_column(PROGRAMMERS_DF, DEVOPS_DF)
