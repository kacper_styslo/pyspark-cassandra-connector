from pyspark.sql.dataframe import DataFrame

# Our
from _jobs_operators import DEVOPS_DF, PROGRAMMERS_DF


def normal_join_example() -> None:
    programmers_and_devops: DataFrame = PROGRAMMERS_DF.join(DEVOPS_DF, on="name")
    programmers_and_devops.show()


normal_join_example()
