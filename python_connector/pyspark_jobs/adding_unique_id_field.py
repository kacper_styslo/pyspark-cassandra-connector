from pyspark.sql import DataFrame
from pyspark.sql import functions as f

# Our
from _jobs_operators import PROGRAMMERS_DF


def adding_unique_id_field() -> None:
    distinct_first_name: DataFrame = PROGRAMMERS_DF.select(PROGRAMMERS_DF["technology"]).distinct()

    print(
        "There are %d technology unique  in the programmers_df dataframe."
        % distinct_first_name.count()
    )

    programmers_df = PROGRAMMERS_DF.withColumn("ROW_ID", f.monotonically_increasing_id())
    programmers_df.orderBy(programmers_df["ROW_ID"].asc()).show(4)


adding_unique_id_field()
