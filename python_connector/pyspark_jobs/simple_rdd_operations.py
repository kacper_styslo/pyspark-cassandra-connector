import collections
from typing import List

from pyspark.rdd import RDD

# Our
from _jobs_operators import SPARK_CONTEXT


def parallelize_and_return_type() -> str:
    rdd_from_list: RDD = SPARK_CONTEXT.parallelize(
        ["Spark", "is", "a", "framework", "for", "Big Data processing"]
    )
    return f"{rdd_from_list} is type {type(rdd_from_list)}"


def map_and_collect() -> None:
    cubed_rdd: RDD = SPARK_CONTEXT.parallelize(list(range(1, 11))).map(lambda num: num ** 3)
    cubed_numbers_all = cubed_rdd.collect()

    for num in cubed_numbers_all:
        print(num)


def filter_numbers_higher_than_100() -> None:
    cubed_rdd_higher_than_100 = (
        SPARK_CONTEXT.parallelize(list(range(1, 11)))
        .map(lambda num: num ** 3)
        .filter(lambda num: num > 100)
    ).collect()
    for num in cubed_rdd_higher_than_100:
        print(num)


def reduce_by_key_example() -> RDD:
    pair_rdd: RDD = SPARK_CONTEXT.parallelize([(1, 2), (3, 4), (3, 6), (4, 5)])
    rdd_reduce = pair_rdd.reduceByKey(lambda x, y: x / y)

    for num in rdd_reduce.collect():
        print("Key {} has {} counts".format(num[0], num[1]))

    return rdd_reduce


def sort_by_key_and_collect() -> None:
    rdd_reduce: RDD = reduce_by_key_example()
    rdd_reduce_sort = rdd_reduce.sortByKey(ascending=False)

    for num in rdd_reduce_sort.collect():
        print("Key {} has {} counts".format(num[0], num[1]))


def count_by_key() -> None:
    total: collections.defaultdict = SPARK_CONTEXT.parallelize(
        [(1, 2), (3, 4), (3, 6), (4, 5)]
    ).countByKey()
    print(f"Type of total is {type(total)}")

    for key, value in total.items():
        print(f"Key: {key}, Value: {value}")


def flat_map_example() -> None:
    nums_rdd: RDD = SPARK_CONTEXT.parallelize(["TEST Test", "kac Kac", "pyspark", "Python python"])
    to_lower_nums_rdd = nums_rdd.flatMap(lambda num: num.lower())
    print(to_lower_nums_rdd.collect())


def remove_stop_words_from_list() -> None:
    stop_words: List[str] = ["pyspark", "Python"]
    split_rdd_no_stop: RDD = SPARK_CONTEXT.parallelize(
        ["TEST Test", "kac Kac", "pyspark", "Python python"]
    ).filter(lambda word: word not in stop_words)
    print(split_rdd_no_stop.collect())


print(parallelize_and_return_type())
map_and_collect()
filter_numbers_higher_than_100()
reduce_by_key_example()
sort_by_key_and_collect()
count_by_key()
flat_map_example()
remove_stop_words_from_list()
