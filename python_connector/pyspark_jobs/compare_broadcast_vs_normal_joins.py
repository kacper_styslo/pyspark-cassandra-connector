import time

from pyspark.sql.functions import broadcast
from pyspark.sql.dataframe import DataFrame

# Our
from _jobs_operators import DEVOPS_DF, PROGRAMMERS_DF


def compare_broadcast_vs_normal_joins() -> None:
    normal_join: DataFrame = PROGRAMMERS_DF.join(DEVOPS_DF, on="name")
    broadcast_join: DataFrame = PROGRAMMERS_DF.join(broadcast(DEVOPS_DF), on="name")

    start_time = time.time()
    normal_count = normal_join.count()
    normal_duration = time.time() - start_time

    start_time = time.time()
    broadcast_count = broadcast_join.count()
    broadcast_duration = time.time() - start_time

    print(f"Normal count {normal_count} in time {normal_duration}")
    print(f"Broadcast count {broadcast_count} in time {broadcast_duration}")


compare_broadcast_vs_normal_joins()
