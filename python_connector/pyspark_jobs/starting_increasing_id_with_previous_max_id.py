from typing import List

from pyspark.sql import DataFrame
from pyspark.sql import functions as f
from pyspark.sql.types import Row

# Our
from _jobs_operators import PROGRAMMERS_DF


def starting_increasing_id_with_previous_max_id() -> None:
    programmers_df: DataFrame = PROGRAMMERS_DF.withColumn("ROW_ID", f.monotonically_increasing_id())

    previous_max_id: List[Row] = programmers_df.select(programmers_df["ROW_ID"]).collect()
    max_id: int = max(max(previous_max_id))

    programmers_df = programmers_df.withColumn("ROW_ID", max_id + f.monotonically_increasing_id())
    programmers_df.select("ROW_ID").show()


starting_increasing_id_with_previous_max_id()
