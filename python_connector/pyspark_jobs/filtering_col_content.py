from pyspark.sql.dataframe import DataFrame

# Our
from _jobs_operators import DEVOPS_DF, PROGRAMMERS_DF


def filtering_col_content() -> None:
    programmers_devops: DataFrame = PROGRAMMERS_DF.union(DEVOPS_DF)
    programmers_devops.select(programmers_devops["salary"]).distinct().show()

    name_df = programmers_devops.filter(
        "length(technology) > 2 and length(technology) < " "6"
    ).filter(~programmers_devops["name"].contains("U"))

    name_df.select(name_df["name"]).distinct().show(2, truncate=False)


filtering_col_content()
