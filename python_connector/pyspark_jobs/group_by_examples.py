from pyspark.sql.dataframe import DataFrame

from _jobs_operators import DEVOPS_DF, PROGRAMMERS_DF, SPARK_CONTEXT
from map_example import map_example


def group_by_examples() -> None:
    all_workers_salary_in_pln: DataFrame = map_example(
        devops_df=DEVOPS_DF, programmers_df=PROGRAMMERS_DF, spark_context=SPARK_CONTEXT
    )

    summed_salary_by_worker = all_workers_salary_in_pln.groupBy("first_last_name").sum(
        "salary_in_pln"
    )
    summed_salary_by_worker.show()

    summed_salary_by_technology = all_workers_salary_in_pln.groupBy("technology_uppercase").sum(
        "salary_in_pln"
    )
    summed_salary_by_technology.show()

    technology_avg_salary = all_workers_salary_in_pln.groupBy("technology_uppercase").avg(
        "salary_in_pln"
    )
    technology_avg_salary.show()


group_by_examples()
