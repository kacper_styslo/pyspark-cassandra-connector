from typing import List

from pyspark.sql import DataFrame
from pyspark.sql import functions as f

# Our
from _jobs_operators import PROGRAMMERS_DF


def get_first_name(split_names: List[str]) -> str:
    """
    Caller: udf_pyspark_example
    The function was created to get first name from split_names data frame column.
    """
    return " ".join(split_names[:-1])


def udf_pyspark_example() -> None:
    """
    udf -> user define functions
    """
    programmers_df: DataFrame = PROGRAMMERS_DF.withColumn(
        "split_names", f.split(PROGRAMMERS_DF["name"], "\s+")
    )

    udf_get_first_name = f.udf(get_first_name)
    udf_get_last_name = f.udf(lambda split_names: " ".join(split_names[1:]))

    programmers_df = programmers_df.withColumn(
        "first_name", udf_get_first_name(programmers_df["split_names"])
    ).withColumn("last_name", udf_get_last_name(programmers_df["split_names"]))

    programmers_df.show()


udf_pyspark_example()
