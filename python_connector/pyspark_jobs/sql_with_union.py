from pyspark.sql.dataframe import DataFrame

# Our
from _jobs_operators import DEVOPS_DF, PROGRAMMERS_DF, SPARK_SESSION


def sql_with_union() -> str:
    programmers_devops: DataFrame = PROGRAMMERS_DF.union(DEVOPS_DF)
    programmers_devops.createOrReplaceTempView("specialist")
    avg_salary = SPARK_SESSION.sql("SELECT avg(salary) FROM specialist").collect()[0]
    return f"The average salary is: %d" % avg_salary


sql_with_union()
