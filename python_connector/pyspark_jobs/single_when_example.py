from typing import Tuple

from pyspark.sql import DataFrame
from pyspark.sql import functions as f

# Our
from _jobs_operators import DEVOPS_DF, PROGRAMMERS_DF
from modifying_df_column import modifying_df_column


def single_when_example() -> None:
    mod_col: Tuple[DataFrame, DataFrame] = modifying_df_column(DEVOPS_DF, PROGRAMMERS_DF)
    programmers_df = mod_col[1]

    programmers_df = programmers_df.withColumn(
        "random_task_id", f.when(programmers_df["first_name"] == "Kacper", f.rand())
    )
    programmers_df.show()


single_when_example()
