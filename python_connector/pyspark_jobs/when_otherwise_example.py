from pyspark.sql import DataFrame
from pyspark.sql import functions as f

# Our
from modifying_df_column import modifying_df_column

from _jobs_operators import DEVOPS_DF, PROGRAMMERS_DF


def when_otherwise_example() -> None:
    mod_col = modifying_df_column(DEVOPS_DF, PROGRAMMERS_DF)
    programmers_df = mod_col[1]

    programmers_df: DataFrame = programmers_df.withColumn(
        "random_task_id",
        f.when(programmers_df["first_name"] == "User", f.rand())
        .when(programmers_df["first_name"] == "Kacper", 2)
        .otherwise(1),
    )
    programmers_df.show()


when_otherwise_example()
