from _jobs_operators import PROGRAMMERS_DF, SPARK_SESSION


def remove_comments_from_csv_file() -> None:
    comment_count = PROGRAMMERS_DF.where(PROGRAMMERS_DF["_c0"].startswith("#")).count()
    if comment_count:
        SPARK_SESSION.read.csv("programmers_data.csv", comment="#")
