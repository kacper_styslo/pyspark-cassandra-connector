import pyspark.sql.functions as f

from _jobs_operators import PROGRAMMERS_DF


def remove_invalid_row() -> None:
    initial_count = PROGRAMMERS_DF.count()
    tmp_fields = f.split(PROGRAMMERS_DF["name"], "\t")
    programmers_df = PROGRAMMERS_DF.withColumn("colcount", f.size(tmp_fields))
    programmers_df_filtered = programmers_df.filter(~(programmers_df["colcount"] == 0))
    final_count = programmers_df_filtered.count()
    print("Initial count: %d\nFinal count: %d" % (initial_count, final_count))


remove_invalid_row()
