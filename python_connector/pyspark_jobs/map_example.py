from typing import Union

from pyspark.rdd import RDD
from pyspark import SparkContext
from pyspark.sql.dataframe import DataFrame

# Our
from _jobs_operators import DEVOPS_DF, PROGRAMMERS_DF, SPARK_CONTEXT


def map_example(**kwargs: Union[DataFrame, SparkContext]) -> DataFrame:
    all_workers: DataFrame = kwargs["programmers_df"].union(kwargs["devops_df"])
    all_workers_rdd: RDD = kwargs["spark_context"].parallelize(all_workers.collect())

    all_workers_rdd.map(lambda x: (x[0] + " , " + x[1], float(x[2]) * 3)).toDF(
        ["ID & Name", "salary " "doubled"]
    ).show()

    all_workers_salary_in_pln: DataFrame = all_workers_rdd.map(
        lambda x: (x[1], float(x[2]) * 4.2, x[3].upper())
    ).toDF(["first_last_name", "salary_in_pln", "technology_uppercase"])

    return all_workers_salary_in_pln


map_example(devops_df=DEVOPS_DF, programmers_df=PROGRAMMERS_DF, spark_context=SPARK_CONTEXT)
