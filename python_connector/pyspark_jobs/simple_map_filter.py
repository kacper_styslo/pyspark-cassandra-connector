from typing import List


def square_numbers_in_list() -> str:
    nums: List[int] = list(range(1, 11))
    squared_nums: List[int] = list(map(lambda num: num ** 2, nums))
    return f"Original list: {nums}, Squared nums in list {squared_nums}"


def only_odd_nums() -> str:
    nums: List[int] = list(range(1, 11))
    odd_nums: List[int] = list(filter(lambda num: num % 2 != 0, nums))
    return f"Original list: {nums}, Only odd nums in list {odd_nums}"


print(square_numbers_in_list())
print(only_odd_nums())
