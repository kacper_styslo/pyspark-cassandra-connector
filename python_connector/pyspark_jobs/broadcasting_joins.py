from pyspark.sql.dataframe import DataFrame
from pyspark.sql.functions import broadcast

# Our
from _jobs_operators import DEVOPS_DF, PROGRAMMERS_DF


def broadcasting_joins() -> None:
    broadcast_df: DataFrame = PROGRAMMERS_DF.join(
        broadcast(DEVOPS_DF), PROGRAMMERS_DF["name"] == DEVOPS_DF["name"]
    )
    broadcast_df.explain()
    broadcast_df.show()


broadcasting_joins()
