import time

# Our
from _jobs_operators import PROGRAMMERS_DF


def caching_df() -> None:
    start_time = time.time()

    programmers_df = PROGRAMMERS_DF.distinct().cache()
    print("Counting %d rows took %f seconds" % (programmers_df.count(), time.time() - start_time))

    start_time = time.time()  # again but cached
    print(
        "Counting %d rows again took %f seconds"
        % (programmers_df.count(), time.time() - start_time)
    )

    # Remove from cache
    print("Is self.programmers_df cached?: %s" % programmers_df.is_cached)
    print("Removing self.programmers_df from cache")
    programmers_df.unpersist()
    print("Is self.programmers_df cached?: %s" % programmers_df.is_cached)


caching_df()
