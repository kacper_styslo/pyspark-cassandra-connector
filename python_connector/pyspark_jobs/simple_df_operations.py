from pyspark.rdd import RDD
from pyspark.sql.dataframe import DataFrame
from pyspark.sql.types import *

from _jobs_operators import DEVOPS_DF, PROGRAMMERS_DF, SPARK_SESSION, SPARK_CONTEXT


def rdd_to_df() -> None:
    list_rdd: RDD = SPARK_CONTEXT.parallelize(
        [("Mona", 20), ("Jennifer", 34), ("John", 30), ("Jim", 26)]
    )
    schema = StructType(
        [StructField("name", StringType(), True), StructField("age", StringType(), True)]
    )
    list_df: DataFrame = SPARK_SESSION.createDataFrame(list_rdd, schema=schema).collect()
    print(f"The type of list_df is: {type(list_df)}")
    print(f"The content of list_df is: {list_df}")

    get_int_age: RDD = list_rdd.map(lambda row: row[1])
    list_rdd_map_df: DataFrame = SPARK_SESSION.createDataFrame(get_int_age, "int").collect()
    print(list_rdd_map_df)


def subsetting_and_cleaning() -> None:
    workers: DataFrame = PROGRAMMERS_DF.union(DEVOPS_DF)

    workers_names = workers.select("name")
    workers_names.show()

    workers_names_no_duplicates = workers_names.drop_duplicates()
    workers_names_no_duplicates.show()

    print(
        f"Numbers of rows with duplicates: {workers_names.count()} and without duplicates: "
        f"{workers_names_no_duplicates.count()}"
    )


def filter_piotr_and_kacper_from_df() -> DataFrame:
    return PROGRAMMERS_DF.filter(~PROGRAMMERS_DF["name"].like("Use%")).select("name").show()


def running_sql_query_programmatically() -> DataFrame:
    PROGRAMMERS_DF.createOrReplaceTempView("programmers_temp")
    programmers_df_technology = SPARK_SESSION.sql(
        """SELECT technology FROM programmers_temp"""
    ).take(4)
    return programmers_df_technology


def sql_query_with_filter() -> DataFrame:
    PROGRAMMERS_DF.createOrReplaceTempView("programmers_temp")
    programmers_df_python = SPARK_SESSION.sql(
        """SELECT * FROM programmers_temp WHERE Technology 
    =='Python'"""
    ).show()
    return programmers_df_python


rdd_to_df()
subsetting_and_cleaning()
print(filter_piotr_and_kacper_from_df())
print(running_sql_query_programmatically())
print(sql_query_with_filter())
