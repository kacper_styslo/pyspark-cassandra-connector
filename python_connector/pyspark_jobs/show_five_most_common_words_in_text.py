# PSL
from os import path
from pathlib import Path
from typing import NoReturn

# Third part
from pyspark import RDD

# Our
from _jobs_operators import SPARK_CONTEXT


def show_five_most_common_words_in_the_text() -> NoReturn:
    text_location: str = path.join(Path(__file__).parent.parent, "db", "data", "lokomotywa.txt")
    five_most_common_words: RDD = SPARK_CONTEXT.textFile(text_location).flatMap(
        lambda words: words.split()).map(lambda word: (word.lower(), 1)).reduceByKey(
        lambda word, amount: word + amount).sortBy(lambda words: -words[1]).take(5)
    print(five_most_common_words)


show_five_most_common_words_in_the_text()
