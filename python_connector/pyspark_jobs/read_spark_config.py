from inspect import cleandoc

from pyspark.sql import SparkSession

from _jobs_operators import SPARK_SESSION


def read_spark_config() -> None:
    print(
        cleandoc(
            f"""
          --------- Spark Config ---------
          Name: {SPARK_SESSION.conf.get('spark.app.name')}
          Driver TCP port: {SPARK_SESSION.conf.get('spark.driver.port')}
          Number of partitions: {SPARK_SESSION.conf.get('spark.sql.shuffle.partitions')}
          --------------------------------
    """
        )
    )


read_spark_config()
