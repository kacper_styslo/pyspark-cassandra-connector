from _jobs_operators import PROGRAMMERS_DF


def first_pipeline() -> None:
    programmers_df_highest_salary = PROGRAMMERS_DF.filter(PROGRAMMERS_DF["salary"] > 2000)
    programmers_df = programmers_df_highest_salary.withColumn(
        "highest salary in pln", programmers_df_highest_salary["salary"] * 4
    )
    programmers_df.show()
    programmers_df.write.json("programmers_df_highest_salary.json", mode="overwrite")


first_pipeline()
