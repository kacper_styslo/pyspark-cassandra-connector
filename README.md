## Pyspark Cassandra Connector

[![Build Status](https://api.travis-ci.com/kacper_styslo/pyspark-cassandra-connector.svg?branch=master)](https://app.travis-ci.com/bitbucket/kacper_styslo/pyspark-cassandra-connector)

# Table of contents

* [Setup](#setup)

## Setup
```
To setup envoriment:
docker-compose up --build -d cassandra-db; python run.py; docker-compose build

To run existing pyspark jobs:
export JOB_NAME="adding_unique_id_field.py"; docker-compose run py-spark

If you add new pyspark job just run:
docker-compose build; export JOB_NAME="broadcasting_joins.py"; docker-compose run py-spark
```

